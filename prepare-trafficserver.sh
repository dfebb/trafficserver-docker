#!/bin/bash

# Script that downloads and verifies Apache Traffic Server
# in preparation for Dockerisation.

app_name=trafficserver
app_version=7.1.4
url=http://archive.apache.org/dist/trafficserver
filename=$app_name-$app_version.tar.bz2

osx(){
  # download dist
  echo "--==[ Downloading $filename from $url ]==--"
  printf "$filename downloading... "
  curl -s -O $url/$filename && echo "OK" || { echo "FAILED"; exit 1; }
  # download sha
  printf "$filename.asc downloading... "
  curl -s -O $url/$filename.sha512 && echo "OK" || { echo "FAILED"; exit 1; }
  # verify sha
  printf "Verifying... "
  shasum -s -a 512 -c $filename.sha512 && echo "OK" || { echo "FAILED"; exit 1; }
  printf "Removing artifacts... "
  rm $filename.sha512 && echo "OK" || { echo "FAILED"; exit 1; }
}

linux(){
  # download dist
  echo "--==[ Downloading $filename from $url ]==--"
  printf "$filename downloading... "
  wget $url/$filename && echo "OK" || { echo "FAILED"; exit 1; }
  # download sha
  printf "$filename.asc downloading... "
  wget $url/$filename.sha512 && echo "OK" || { echo "FAILED"; exit 1; }
  # verify sha
  printf "Verifying... "
  shasum -s -a 512 -c $filename.sha512 && echo "OK" || { echo "FAILED"; exit 1; }
  printf "Removing artifacts... "
  rm $filename.sha512 && echo "OK" || { echo "FAILED"; exit 1; }
}

clear
# depending on OS type, run certain commands
case "$OSTYPE" in
  darwin*)  osx ;;
  linux*)   linux ;;
  *)        echo "OS $OSTYPE not supported." ;;
esac

echo "DONE"
