#!/bin/bash

# Script that extracts tar and builds app on local machine.

app_name=trafficserver
app_version=7.1.4
git_url=https://github.com/apache/trafficserver/
target_dir=/Users/ts-damiano.febbrarin/Work/trafficserver #/opt/ts

clear
echo "--==[ Building $app_name version $app_version ]==--"

#printf "Extracting $app_name-$app_version.tar... "
#tar -xf $app_name-$app_version.tar* && echo "OK" || { echo "FAILED"; exit 1; }

# build from source
git config --global advice.detachedHead false # turn the detached message off
printf "Cloning $git_url (branch=$app_version)... "
git clone --quiet $git_url --depth=1 --branch $app_version --single-branch $app_name  && echo "OK" || { echo "FAILED"; exit 1; }
git config --global advice.detachedHead true # turn the detached message back on

echo "Making... "
cd $app_name
autoreconf -if
./configure --prefix=$target_dir
cd $target_dir
make
make check
make install

# create simlink exec
# cd $target_dir
# sudo bin/traffic_server -R 1
